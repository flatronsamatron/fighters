import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  try{
    Object
    .keys(fighter)
    .forEach(key => {
      if( key === 'source' ){
        const imgElement = createElement({
          tagName: 'img',
          className: 'fighter___fighter-image',
          attributes: { 
            src: fighter['source'],
            title: fighter['name'],
            alt: fighter['name'], 
          }
        });
        fighterElement.append(imgElement)
      } else if (key === '_id'){
        return
      } else {
        const p = createElement({
          tagName: 'p',
          className: 'fighter-info',
        });
        p.innerText = key + ': ' + fighter[key]
        fighterElement.append(p)
      }
    });
  } catch(e){
    console.log(e)
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

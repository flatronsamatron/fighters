import { controls } from '../../constants/controls';

let oneBlock = false;
let twoBlock = false;

let widthL = 100;
let widthR = 100;

export async function fight(firstFighter, secondFighter) {

  const leftLifeBar = document.querySelector('#left-fighter-indicator')
  const rightLifeBar = document.querySelector('#right-fighter-indicator')

  const {
    PlayerOneAttack,
    PlayerOneBlock,
    PlayerTwoAttack,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination
  } = controls;

  let firstFighterAttacker = firstFighter
  let secondFighterAttacker = secondFighter

  document.addEventListener('keydown', (e) => {
    if(e.code === PlayerOneAttack && !oneBlock){
      let damage = getDamage(firstFighterAttacker, secondFighterAttacker)
      let healthBar = (damage*100)/firstFighter.health
      widthR = widthR - healthBar
      rightLifeBar.style.width = widthR + '%'

      secondFighterAttacker.health = secondFighterAttacker.health - damage
      if(secondFighterAttacker.health <= 0){
        rightLifeBar.style.width = 0
        console.log(1)
      }
    }
    if(e.code === PlayerOneBlock){
      oneBlock = true
    }
    if(e.code === PlayerTwoAttack && !twoBlock){
      let damage = getDamage(secondFighterAttacker, firstFighterAttacker)
      let healthBar = (damage*100)/secondFighter.health
      widthL = widthL - healthBar
      leftLifeBar.style.width = widthL + '%'

      firstFighterAttacker.health = firstFighterAttacker.health - damage
      if(firstFighterAttacker.health <= 0){
        leftLifeBar.style.width = 0
        console.log(2)
      }
    }
    if(e.code === PlayerTwoBlock){
      twoBlock = true
    }
  });

  document.addEventListener('keyup', (e) => {
    if(e.code === PlayerOneBlock){
      oneBlock = false
    }
    if(e.code === PlayerTwoBlock){
      twoBlock = false
    }
  });

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender)
  if((oneBlock || twoBlock) && damage>0) {
    return damage
  } else {
    return getHitPower(attacker)
  }
}



export function getHitPower(fighter) {
  let criticalHitChance = 1 + Math.random() * (2 - 1)
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  let dodgeChance  = 1 + Math.random() * (2 - 1)
  return fighter.defense * dodgeChance 
}
